package com.example.android.find_your_flat;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Switch;
import android.widget.ViewAnimator;

import com.example.android.find_your_flat.MySearchCriteriaObject.SearchCriteria;

public class Tab1 extends android.support.v4.app.Fragment{
    public static final  String TAG = "tab1";
    private SearchCriteria criteria;
    private ViewAnimator animator;
    final Handler timerHandler = new Handler();
    Runnable timerRunnable;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        Bundle bundle = getArguments();
        if (bundle != null) {
            criteria = bundle.getParcelable("searchCriteria");
        }

        final View inflatedView = inflater.inflate(R.layout.tab1, container, false);
        //initialize animator and images
        animator = (ViewAnimator) inflatedView.findViewById(R.id.animator);

        //initialization of buttons and settting two buttons (Flat and Buy) in default position as "checked"

        final Switch switch1 = (Switch) inflatedView.findViewById(R.id.switchBr);
        if (switch1 != null) {
            switch1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (switch1.isChecked()) {
                        criteria.setBuy(false);
                    } else {
                        criteria.setBuy(true);
                    }
                }
            });
        }
        final Switch switch2 = (Switch) inflatedView.findViewById(R.id.switchFh);
        if (switch2 != null) {
            switch2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (switch2.isChecked()) {
                        criteria.setFlat(false);
                    } else {
                        criteria.setFlat(true);
                    }
                }
            });
        }

        return inflatedView;
    }
    public void onPause() {
        super.onPause();
        // this will ensure that animation does not run after the Tab1 view was destoyed and its
        // messages will not populate the call stack
        timerHandler.removeCallbacks(timerRunnable);
    }

    public void onResume() {
        super.onResume();

        //animation config
        final Animation inAnim = new AlphaAnimation(0, 1);
        inAnim.setDuration(2000);
        final Animation outAnim = new AlphaAnimation(1, 0);
        outAnim.setDuration(2000);
        animator.setInAnimation(inAnim);
        animator.setOutAnimation(outAnim);
        animator.setAnimateFirstView(true);
        //runs in a separate thread

        timerRunnable = new Runnable() {

            @Override
            public void run() {
                animator.showNext();
                timerHandler.postDelayed(this, 2000);
            }
        };
        timerHandler.postDelayed(timerRunnable, 2000);
    }
}




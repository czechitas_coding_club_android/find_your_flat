package com.example.android.find_your_flat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.android.find_your_flat.MySearchCriteriaObject.SearchCriteria;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import static android.content.Context.LOCATION_SERVICE;


/**
 * An activity that displays a Google map with a marker (pin) to indicate a particular location.
 */
public class Tab2 extends Fragment implements OnMapReadyCallback {
    private SearchCriteria criteria;
    private FragmentTransaction transaction;
    private LocationManager locationManager;
    private String provider; //Returns the name of the provider that best meets the given criteria.
    private LocationListener locationListener;
    double longitude;
    double latitude;
    public static final String TAG=Tab2.class.getSimpleName();

    GoogleMap mGoogleMap;
    View mView;
    public Tab2(){
        //required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        Bundle bundle = getArguments();
        if (bundle != null) {
            criteria = bundle.getParcelable("searchCriteria");
            Log.i(TAG, "parcelable/bundle not null, Location from bundle is = "+ criteria.getLatitude()+","+criteria.getLongitude());
        }

    }

    private Location getLastKnownLocation() {
        locationManager = (LocationManager)getContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if ( ContextCompat.checkSelfPermission( getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {

                ActivityCompat.requestPermissions( getActivity(), new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
            return false;
        } else {
            return true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        locationListener = new LocationListener() {
                            public void onLocationChanged(Location location) {
                                // Called when a new location is found by the network location provider.
                                // TODO: implement map update on location change
                                //makeUseOfNewLocation(location);
                            }

                            public void onStatusChanged(String provider, int status, Bundle extras) {}

                            public void onProviderEnabled(String provider) {}

                            public void onProviderDisabled(String provider) {}
                        };

                        //Request location updates:
                        locationManager.requestLocationUpdates(provider, 400, 1, locationListener);
                    }

                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    Toast.makeText(getContext(), "Location permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");

        // Retrieve an instance of the LocationManager
        locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // check if enabled and if not send user to the GSP settings (another option is to display a dialog maybe TODO later)
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
        Criteria locationCriteria = new Criteria();
        locationCriteria.setAccuracy(Criteria.NO_REQUIREMENT);
        locationCriteria.setPowerRequirement(Criteria.NO_REQUIREMENT);

        provider = locationManager.getBestProvider(locationCriteria, true);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            checkLocationPermission();
            //    ActivityCompat#requestPermissions
            //   here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }

        Location location = getLastKnownLocation();
        if (location != null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            if (criteria != null) {
                criteria.setLatitude(latitude);
                criteria.setLongitude(longitude);
                Log.i(TAG, "Location status is = "+ criteria.getLatitude()+","+criteria.getLongitude());
            }

        }

        if (mView != null) {
            ViewGroup viewGroupParent = (ViewGroup) mView.getParent();
            if (viewGroupParent != null)
                viewGroupParent.removeView(mView);
        } else {
            try {
                mView = inflater.inflate(R.layout.tab2, container, false);
                Log.i(TAG, "onCreateView: tab2 fragment inflated");
            } catch (Exception e) {
                Log.i(TAG, "exception mView inflation" + e.getMessage());
            }
        }

        Button button = (Button) mView.findViewById(R.id.button_search);

        if (button != null) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fetchRealEstateEntities(criteria);
                    SimpleOrientationActivity myOrientationEventListener = new SimpleOrientationActivity();
                }
            });
        }


        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        Log.i(TAG, "onViewCreated");
        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        transaction = getChildFragmentManager().beginTransaction();
        if(fragment == null){
            SupportMapFragment newFragment= new SupportMapFragment();
            transaction.add(R.id.map, newFragment);
            newFragment.getMapAsync(this);
        }
        else {
            Log.d(TAG, "onViewCreated: map fragment created");
            transaction.replace(R.id.map, fragment);
            fragment.getMapAsync(this);
        }
        transaction.commit();
    }
    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i(TAG, "onMapReady");
        //Callback interface for when the map is ready to be used.

        MapsInitializer.initialize(getContext());
        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude)));

        CameraPosition cameraPosition = CameraPosition
                .builder()
                .target(new LatLng(latitude, longitude))
                .zoom(16)
                .bearing(8)
                .tilt(45)
                .build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG, "onDestroyView");
    }

    public void fetchRealEstateEntities(SearchCriteria criteria) {
        // call AsynTask to perform network operation on separate thread
        String myUrl = parseSearchCriteria(criteria);
        new RestTask().execute(myUrl);
    }

    private static String parseSearchCriteria(SearchCriteria c)  {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.sreality.cz")
                .appendPath("api")
                .appendPath("cs")
                .appendPath("v2")
                .appendPath("estates")
                .appendQueryParameter("per_page", "2");
        if (c.isFlat()){
            builder.appendQueryParameter("category_main_cb", "1");
        } else {
            builder.appendQueryParameter("category_main_cb", "2");
        }
        String myUrl = builder.build().toString();
        Log.i(TAG, myUrl);
        return myUrl;
    }
}



package com.example.android.find_your_flat;

/**
 * Created by andronov on 21-May-17.
 */

public class Property {
    String name;
    String type;
    String size;
    String locality;
    int item_img;
    int price;
    double lat;
    double lon;

    public Property() {
    }

    public Property(String name, String type, String size, String locality, int item_img, int price, double lat, double lon) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.locality = locality;
        this.item_img = item_img;
        this.price = price;
        this.lat = lat;
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public int getItem_img() {
        return item_img;
    }

    public void setItem_img(int item_img) {
        this.item_img = item_img;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}

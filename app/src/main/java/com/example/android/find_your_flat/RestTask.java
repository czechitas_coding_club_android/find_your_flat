package com.example.android.find_your_flat;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class RestTask extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String ... url) {

        String result = "";
        try {
            result = GET(url[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
    }

    public boolean isConnected(Context cnt){
        ConnectivityManager connMgr = (ConnectivityManager) cnt.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    public static String GET(String myUrl) throws IOException {
        InputStream inputStream = null;
        String result = "";

        URL url = null;
        try {
            url = new URL(myUrl);
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        }
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        int responseCode = urlConnection.getResponseCode();
        if (responseCode == 200)  {
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = readStream(in);
                in.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        urlConnection.disconnect();

        return result;
    }

    private static String readStream(InputStream in) throws IOException {
        JSONObject jsonReader = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder result = new StringBuilder();
        String line = "";
        while((line = reader.readLine()) != null) {
            result.append(line);
        }
        //Log.i("Tab2", result.toString());

        try {
            jsonReader = new JSONObject(result.toString());
            if (jsonReader != null){
                JSONObject embedded = jsonReader.getJSONObject("_embedded");
                JSONArray estates = (JSONArray)embedded.getJSONArray("estates");

                ArrayList<Property> resultsArray = new ArrayList<>();
                for (int i=0; i<estates.length(); i++){

                    JSONObject propertyObj =  estates.getJSONObject(i);
                    Property p = new Property();
                    p.setName(propertyObj.getString("name"));
                    p.setPrice(propertyObj.getInt("price"));
                    JSONObject flatGps = propertyObj.getJSONObject("gps");
                    p.setLon(flatGps.getDouble("lon"));
                    p.setLat(flatGps.getDouble("lat"));
                    resultsArray.add(p);
                }



                Log.i("Tab2","resultsArray 1= " + resultsArray.get(0).getName().toString() );
                Log.i("Tab2","resultsArray 2= " + resultsArray.get(1).getName().toString() );

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }




        return result.toString();

    }
}
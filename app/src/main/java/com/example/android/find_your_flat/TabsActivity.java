package com.example.android.find_your_flat;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.android.find_your_flat.MySearchCriteriaObject.SearchCriteria;

public class TabsActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    FragmentManager fm = getSupportFragmentManager();
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private Bundle bundle = new Bundle();
    private ViewPager mViewPager;

    public void onBuyRentSelected () {

        bundle.putBoolean("buyrentchanged", true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SearchCriteria mySearchCriteria = new SearchCriteria();

        bundle.putParcelable("searchCriteria", mySearchCriteria);
        bundle.putBoolean("buyrentchanged", false); //default value

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tabs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
            switch (position){
                case 0:
                    //Tab1 tab1 = new Tab1();
                    return new Tab1();
                    /*tab1.setArguments(bundle);
                    return tab1;*/
                case 1:
                    /*Tab2 tab2 = new Tab2();
                    tab2.setArguments(bundle);*/
                    return new Tab2();
                case 2:
                   /*Tab3 tab3 = new Tab3();
                    tab3.setArguments(bundle);*/
                    return new Tab3();
                case 3:
                    /*Tab4 tab4 = new Tab4();
                    tab4.setArguments(bundle);*/
                    return new Tab4();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Type";
                case 1:
                    return "Location";
                case 2:
                    return "Cost";
                case 3:
                    return "Results";
            }
            return null;
        }

        /* Here we can finally safely save a reference to the created
        // Fragment, no matter where it came from (either getItem() or
        // FragmentManger). Simply save the returned Fragment from
        // super.instantiateItem() into an appropriate reference depending
        // on the ViewPager position.*/
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
            // save the appropriate reference depending on position
            switch (position) {
                case 0:
                    Tab1 tab1 = (Tab1) createdFragment;
                    if (tab1.getArguments() != null) break;
                    tab1.setArguments(bundle);
                    break;
                case 1:
                    Tab2 tab2 = (Tab2) createdFragment;
                    if (tab2.getArguments() != null) break;
                    tab2.setArguments(bundle);
                    break;
                case 2:
                    Tab3 tab3 = (Tab3) createdFragment;
                    if (tab3.getArguments() != null) break;
                    tab3.setArguments(bundle);
                    break;
                case 3:
                    Tab4 tab4 = (Tab4) createdFragment;
                    if (tab4.getArguments() != null) break;
                    tab4.setArguments(bundle);
                    break;
            }
            return createdFragment;
        }

        private String getFragmentTag(int viewPagerId, int fragmentPosition)
        {
            return "android:switcher:" + viewPagerId + ":" + fragmentPosition;
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen for landscape and portrait and set portrait mode always
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

}

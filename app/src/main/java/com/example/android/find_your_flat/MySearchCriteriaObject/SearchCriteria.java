package com.example.android.find_your_flat.MySearchCriteriaObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


public class SearchCriteria implements Parcelable {
    public static final String TAG=SearchCriteria.class.getSimpleName();

    boolean isBuy; //false = rent, true = buy
    boolean isFlat; //false = house, true = flat
    double minCost;
    double maxCost;
    double latitude;
    double longitude;

    public SearchCriteria() {
        Log.i(TAG, "++++++ SearchCriteria() constructor called ++++++");
        isBuy = true;
        isFlat = true;
        minCost = 0;
        maxCost = 10000000;
        latitude = 0;
        longitude = 0;
    }

    public SearchCriteria(Parcel parcel) {
        Log.i(TAG, "++++++ SearchCriteria(Parcel parcel) constructor called ++++++");
        this.isBuy = parcel.readByte()==1?true:false;
        this.isFlat = parcel.readByte()==1?true:false;
        this.minCost = parcel.readDouble();
        this.maxCost = parcel.readDouble();
        this.latitude = parcel.readDouble();
        this.longitude = parcel.readDouble();
    }

    public boolean isBuy() {
        return isBuy;
    }

    public void setBuy(boolean buy) {
        isBuy = buy;
    }

    public boolean isFlat() {
        return isFlat;
    }

    public void setFlat(boolean flat) {
        isFlat = flat;
    }

    public double getMinCost() {
        return minCost;
    }

    public void setMinCost(double minCost) {
        this.minCost = minCost;
    }

    public double getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(double maxCost) {
        this.maxCost = maxCost;
    }

    public double getLatitude() {
        return latitude;
    }
    public double getLongitude() {
        return longitude;
    }

    public void setLatitude(double lat) {
        this.latitude = lat;
    }
    public void setLongitude(double lng) {this.longitude = lng; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        Log.i(TAG, "++++++ writeToParcel method called ++++++");
        parcel.writeByte(this.isBuy==true?(byte)1:(byte)0);
        parcel.writeByte(this.isFlat==true?(byte)1:(byte)0);
        parcel.writeDouble(this.minCost);
        parcel.writeDouble(this.maxCost);
        parcel.writeDouble(this.latitude);
        parcel.writeDouble(this.longitude);
    }

    public static final Parcelable.Creator<SearchCriteria> CREATOR = new Parcelable.Creator<SearchCriteria>() {

        public SearchCriteria createFromParcel(Parcel in) {
            Log.i(TAG, "++++++ createFromParcel SearchCriteria constructor called ++++++");
            return new SearchCriteria(in);
        }

        public SearchCriteria[] newArray(int size) {
            Log.i(TAG, "++++++ SearchCriteria[] newArray called ++++++");
            return new SearchCriteria[size];
        }
    };


}

package com.example.android.find_your_flat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.find_your_flat.MySearchCriteriaObject.SearchCriteria;

import org.florescu.android.rangeseekbar.RangeSeekBar;


public class Tab3 extends android.support.v4.app.Fragment {
    public static final String TAG=Tab3.class.getSimpleName();
    public static final double RENT_MAX_PRICE = 40000; // in czk
    public static final double BUY_MAX_PRICE = 10000000; // in czk
    public static final double MIN_PRICE = 0;

    Bundle bundle;
    private SearchCriteria criteria;
    RangeSeekBar seekBar;
    TextView minText;
    TextView maxText;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        final View inflatedView = inflater.inflate(R.layout.tab3, container, false);

        bundle = getArguments();
        if (bundle != null) {
            criteria = bundle.getParcelable("searchCriteria");
        }

        //initialize seekbar and textfield
        seekBar = (RangeSeekBar) inflatedView.findViewById(R.id.rangeSeekBar);
        if (criteria.isBuy()) {
            seekBar.setRangeValues(MIN_PRICE, BUY_MAX_PRICE);
        } else {
            seekBar.setRangeValues(MIN_PRICE, RENT_MAX_PRICE);
        }

        minText = (TextView) inflatedView.findViewById(R.id.minValueText);
        maxText = (TextView) inflatedView.findViewById(R.id.maxValueText);

        return inflatedView;

    }

    String formatObject(Object o){
        Double objValue = new Double(o.toString());
        return String.format("%,d", objValue.intValue()).replace(",", " ");
    }

    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

        if(criteria !=null ) {
            if (criteria.isBuy()) {
                seekBar.setRangeValues(MIN_PRICE, BUY_MAX_PRICE);

            } else {
                seekBar.setRangeValues(MIN_PRICE, RENT_MAX_PRICE);
            }
            criteria.setMaxCost((double) seekBar.getSelectedMaxValue());
            criteria.setMinCost((double) seekBar.getSelectedMinValue());
            bundle.putBoolean("buyrentchanged", false);

            minText.setText(formatObject(criteria.getMinCost()));
            maxText.setText(formatObject(criteria.getMaxCost()));
        }

        seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
                minText.setText(formatObject(minValue));
                String x = minValue.toString();
                if(criteria !=null ) {
                    criteria.setMinCost(new Double(minValue.toString()));
                }

                maxText.setText(formatObject(maxValue));
                if(criteria !=null ) {
                    criteria.setMaxCost(new Double(maxValue.toString()));
                }

            }

        });

    }


}






